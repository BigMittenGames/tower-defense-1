﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Main : MonoBehaviour {

	//multiple spawn points per wave
	//Should hit every enemy

	//----------Public Variables
	public Level1 newLevel;
	public Character mainCharacter; //Is only here to spawn the initial character
	public List <Character> players = new List <Character> ();
	public Text waveDisplay, towerDisplay, livesDisplay, resourceDisplay, highScore, bigText;
	//public GameObject enemy;
	public List <Vector3> enemySpawners;
	int lastSpawn = 0;
	float spawnDelay = .01f;
	float spawnCounter;
	public Vector2 towerPosition, mapSize;
	public int enemyCounter = 0;
	public Material towerMaterial, groundMaterial;


	public int startingWaypoint;

	float xLimit = 20, yLimit = 13, cameraTimer;

	bool gameEnded;
	bool tempBool;
	float tempFloat;
	GameObject tempObject;
	Vector2 tempVector2;
	Vector3 tempVector3;
	Vector3 cameraPosition, lastCameraPosition;

	int towerHealth = 100;
	int lives = 3;
	public GameObject towerGO;

	int enemiesToSpawn, waveEnemies = 500;
	int wave = 0, resource = 0;
	float waveDelay = 2;
	float endGameDelay = 5;
	Vector3 waveSpawnPoint = new Vector3 ();
	public List <GameObject> buildingGOs;
	float closestDist;
	public GameObject model;
	List <MeshRenderer> meshes = new List <MeshRenderer> ();
	MeshRenderer tempMesh;

	//----------Building Variables
	List <Building> buildings = new List <Building> ();
	List <GameObject> tempBuildings = new List <GameObject> ();
	int buildingCost = 300;
	public List <int> selectedBuildings = new List<int> ();
	List <float> groundBuildingRanges = new List <float> { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
	List <int> groundBuildingDamages = new List <int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
	List <float> groundBuildingAttackDelays = new List <float> { .25f, .25f, .25f, .25f, .25f, .25f, .25f, .25f, .25f, .25f, .25f, .25f, .25f, .25f, .25f};

	public Projectile arrow;
	List <Projectile> arrows = new List <Projectile> ();
	List <Elevator> elevators = new List<Elevator> ();
	public Elevator waterSpot;

	//----------Temperary Variables
	int i1, i2, j1, j2; //i is for items in the update function, j is for other functions.  The number represents the depth
	Vector2 v2a, v2b;

	//----------Player Input Variables
	/*List <float> playersX = new List <float> (12); //Moves the player horizontally
	List <float> playersY = new List <float> (12); //Moves the player vertically
	List <float> playersLookX = new List <float> (12); //Controls the players horizontal look
	List <float> playersLookY = new List <float> (12); //Controls the players vertical look 
	List <bool> playersAttack = new List <bool> (12); //Attacks
	List <bool> playersBuild = new List <bool> (12); //Opens the build menu
	List <bool> playersBuildUp = new List <bool> (12);
	List <bool> playersBuildUpDown = new List <bool> (12);
	List <bool> playersBuildDown = new List <bool> (12);
	List <bool> playersBuildDownDown = new List <bool> (12);
	List <bool> playersBuildLeft = new List <bool> (12);
	List <bool> playersBuildLeftDown = new List <bool> (12);
	List <bool> playersBuildRight = new List <bool> (12);
	List <bool> playersBuildRightDown = new List <bool> (12);*/

	//----------Enemy Variables
	List <int> spawnedEnemies = new List <int> ();
	//public List <Enemy> enemies2;
	public List <int> enemyIDs = new List<int> ();
	List <int> enemyHPs = new List<int> ();
	List <GameObject> enemyGOs = new List<GameObject> ();
	List <MeshRenderer> enemyRenderers = new List<MeshRenderer> ();
	List <float> enemyStepDistance = new List <float> ();
	List <float> enemyDeathTimer = new List <float> ();
	List <int> enemyWaypoints = new List <int> ();
	List <int> enemySteps = new List <int> ();
	List <Vector2> enemyPos = new List <Vector2> ();
	public List <Vector2> enemytargets = new List <Vector2> ();
	List <Vector2> deadEnemyPos = new List <Vector2> ();
	List <Vector2> deadEnemytargets = new List <Vector2> ();
	List <GameObject> deadEnemyGOs = new List<GameObject> ();

	//--------------------------------------------------Start

	void Start () {
		Physics.autoSimulation = false; //Turns off physics, because we will be calculating it ourselves
		spawnDelay = newLevel.waves[wave].spawnDelay;
		transform.position = newLevel.waves [wave].cameraPosition;
		lastCameraPosition = transform.position;
		waveDisplay.text = "Wave: 0";
		if (PlayerPrefs.GetInt ("HighScore", 0) > 0) {
			highScore.text = "High Score: Wave " + PlayerPrefs.GetInt ("HighScore", 0);
		} else {
			highScore.text = "";
		}
		AddResource (500);
		//AddCharacter ();
		livesDisplay.text = "Lives: " + lives;
		elevators.Add (waterSpot);
		tempMesh = NewMesh ();
		tempMesh.transform.localScale = new Vector3 (5, 5, 5);
		tempMesh.transform.position = new Vector3 (towerPosition.x, 0, towerPosition.y);
		tempMesh.transform.rotation = Quaternion.Euler (new Vector3 (30, 0, 0));
		tempMesh.material = towerMaterial;
		towerGO = tempMesh.gameObject;
		tempMesh = NewMesh ();
		tempMesh.transform.localScale = new Vector3 (mapSize.x, mapSize.y, 1);
		tempMesh.transform.position = new Vector3 (0, 0, mapSize.y * -.5f);
		tempMesh.transform.rotation = Quaternion.Euler (new Vector3 (90, 0, 0));
		tempMesh.material = groundMaterial;
	}

	//--------------------------------------------------Update

	void Update () {

		//--------------------------------------------------Camera Controls
		if (cameraTimer > 0) {
			cameraTimer -= Time.deltaTime;
			if (cameraTimer <= 0) {
				cameraTimer = 0;
				transform.position = cameraPosition;
				lastCameraPosition = cameraPosition;
			} else {
				tempFloat = cameraTimer / 5;
				transform.position = Vector3.Lerp (cameraPosition, lastCameraPosition, tempFloat * tempFloat);
			}
		}

		//--------------------------------------------------End State Check
		if (gameEnded) {
			if (endGameDelay == 5) {
				endGameDelay -= Time.deltaTime;
				waveDisplay.text = "You win!";
				PlayerPrefs.SetInt ("HighScore", wave);
			} else if (endGameDelay > 0) {
				endGameDelay -= Time.deltaTime;
			} else {
				RestartGame ();
			}
		} else if (!towerGO) {
			if (endGameDelay == 5) {
				endGameDelay -= Time.deltaTime;
				string addon = "";
				if (wave < 5) {
					addon = "You kinda suck at this game.";
				} else if (wave < 10) {
					addon = "You're getting invested, but you still have a ways to go.";
				} else if (wave < 25) {
					addon = "Impressive.";
				} else if (wave < 50) {
					addon = "Why would you play an unfinished game for so long?";
				} else if (wave < 100) {
					addon = "You're very good.";
				} else if (wave < 250) {
					addon = "You have my respect.";
				}
				waveDisplay.text = "You lost on wave " + wave + ".  " + addon;
				PlayerPrefs.SetInt ("HighScore", wave);
			} else if (endGameDelay > 0) {
				endGameDelay -= Time.deltaTime;
			} else {
				RestartGame ();
			}
			return; //If there's no tower, stop the match
		}

		//--------------------------------------------------Looks for new characters

		i1 = Input2.Instance.NewPlayerPress ();
		if (i1 != -1) { //NewPlayerPress() returns -1 if no player is found.  If a player is found, it will return the new player number
			AddCharacter ();
		}

		//--------------------------------------------------Player Controls



		for (i1 = 0; i1 < players.Count; i1++) {
			Input2.Instance.UpdatePlayerInputs (i1);
			if (players [i1].hp > 0) {
				tempVector2 = players [i1].v2;
				tempBool = false; //Stores whether or not the player has moved on this frame
				if (Input2.Instance.x != 0) {
					players [i1].v2.x += Input2.Instance.x * Time.deltaTime; tempBool = true; //Should also have a speed varieable based on character
					if (players [i1].transform.localScale.x > 0 && Input2.Instance.x < 0) {	
						players [i1].transform.localScale = new Vector3 (players [i1].transform.localScale.x * -1f, players [i1].transform.localScale.y, players [i1].transform.localScale.z);}
					if (players [i1].transform.localScale.x < 0 && Input2.Instance.x > 0) {
						players [i1].transform.localScale = new Vector3 (players [i1].transform.localScale.x * -1f, players [i1].transform.localScale.y, players [i1].transform.localScale.z);}
				}
				if (Input2.Instance.y != 0) {
					players [i1].v2.y += Input2.Instance.y * Time.deltaTime; tempBool = true; //Should also have a speed varieable based on character
				}

				/*if (Input.GetKey (KeyCode.UpArrow) || Input.GetKey (KeyCode.W)) {
					players [i1].v2.y += Time.deltaTime; tempBool = true;}
				if (Input.GetKey (KeyCode.DownArrow) || Input.GetKey (KeyCode.S)) {
					players [i1].v2.y -= Time.deltaTime; tempBool = true;}
				if (Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.A)) {
					players [i1].v2.x -= Time.deltaTime; tempBool = true;
					if (players [i1].transform.localScale.x > 0) {	
						players [i1].transform.localScale = new Vector3 (players [i1].transform.localScale.x * -1f, players [i1].transform.localScale.y, players [i1].transform.localScale.z);}}
				if (Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.D)) {
					players [i1].v2.x += Time.deltaTime; tempBool = true;
					if (players [i1].transform.localScale.x < 0) {
						players [i1].transform.localScale = new Vector3 (players [i1].transform.localScale.x * -1f, players [i1].transform.localScale.y, players [i1].transform.localScale.z);}}
				*/
				/*if (players [i1].v2.x > xLimit) {
					players [i1].v2.x = xLimit;
				} else if (players [i1].v2.x < -xLimit) {
					players [i1].v2.x = -xLimit;}
				if (players [i1].v2.y > yLimit) {
					players [i1].v2.y = yLimit;
				} else if (players [i1].v2.y < -yLimit) {
					players [i1].v2.y = -yLimit;}*/
				if (tempBool) { //is true if the player has moved on this frame
					tempVector3.x = players [i1].v2.x;
					tempVector3.y = 0; //Elevation (players [i1].v2);
					tempVector3.z = players [i1].v2.y;
					players [i1].transform.position = tempVector3;
					players [i1].currentStepDistance += (tempVector2 - players [i1].v2).magnitude;
					if (players [i1].currentStepDistance >= players [i1].stepDistance) {
						players [i1].currentStepDistance -= players [i1].stepDistance;
						players [i1].walkSprites [players [i1].step].SetActive (false);
						players [i1].step++;
						if (players [i1].step >= players [i1].walkSprites.Count) {
							players [i1].step = 1;
						}
						players [i1].walkSprites [players [i1].step].SetActive (true);
					}
				} else if (players [i1].step != 0) { //The player hasn't moved, so we're going to see if they are on the 'still' frame
					players [i1].currentStepDistance = 0;
					players [i1].walkSprites [players [i1].step].SetActive (false);
					players [i1].step = 0;
					players [i1].walkSprites [players [i1].step].SetActive (true);
				}

				if (Input2.Instance.attack) {
					NormalAttack (i1);
				}

				if (selectedBuildings [i1] != 0) {
					if (players [i1].transform.localScale.x < 0) { //Facing left
						tempVector3.x = Mathf.Round (players [i1].v2.x - 1);
					} else { //Facing Right
						tempVector3.x = Mathf.Round (players [i1].v2.x + 1);
					}
					tempVector3.y = 0;
					tempVector3.z = Mathf.Round(players [i1].v2.y);
					tempBuildings [i1].transform.position = tempVector3;
					if (Input2.Instance.jump) {
						selectedBuildings [i1] = 0;
						Destroy (tempBuildings [i1]);
					} else if (Input2.Instance.attack) {
						RemoveResource (buildingCost);
						tempBool = false;
						for (i2 = 0; i2 < buildings.Count; i2++) {
							if (buildings [i2].transform.position == tempVector3) {
								tempBool = true;
								i2 = buildings.Count;
							}
						}
						if (!tempBool) {
							buildings.Add (tempBuildings [i1].GetComponent <Building> ());
							buildings [buildings.Count - 1].v2.x = tempVector3.x;
							buildings [buildings.Count - 1].v2.y = tempVector3.z;
						}
						tempBuildings [i1] = null;
						selectedBuildings [i1] = 0;
					}
				} else if (resource >= buildingCost) {
					if (Input2.Instance.buildLeft) {
						selectedBuildings [i1] = 1;
						tempBuildings [i1] = (GameObject)Instantiate (buildingGOs [1], players [i1].transform.position, Quaternion.identity);
					}
				}
				
				/*if (players [i1].building) {
					if (players [i1].transform.localScale.x < 0) { //Facing left
						tempVector3.x = Mathf.Round (players [i1].v2.x - 1);
					} else { //Facing Right
						tempVector3.x = Mathf.Round (players [i1].v2.x + 1);
					}
					tempVector3.y = 0;
					tempVector3.z = Mathf.Round(players [i1].v2.y);
					players [i1].building.transform.position = tempVector3;
					if (!Input.GetKey (KeyCode.E)) {
						//Places building
						tempBool = false;
						for (i2 = 0; i2 < buildings.Count; i2++) {
							if (buildings [i2].transform.position == tempVector3) {
								tempBool = true;
								i2 = buildings.Count;
							}
						}
						if (!tempBool) {
							buildings.Add (players [i1].building.GetComponent <Building> ());
							buildings [buildings.Count - 1].v2.x = tempVector3.x;
							buildings [buildings.Count - 1].v2.y = tempVector3.z;
						}
						players [i1].building = null;
					}
				} else if (resource >= buildingCost && Input.GetKeyDown (KeyCode.E)) {
					RemoveResource (buildingCost);
					players [i1].building = (GameObject)Instantiate (buildingGOs [1], players [i1].transform.position, Quaternion.identity);
				} if (resource >= buildingCost && Input.GetKeyDown (KeyCode.R)) {
					RemoveResource (buildingCost);
					players [i1].building = (GameObject)Instantiate (buildingGOs [2], players [i1].transform.position, Quaternion.identity);
				}*/
			}
		}

		//--------------------------------------------------Spawns Enemies
		if (enemiesToSpawn > 0) {
			spawnCounter -= Time.deltaTime;
			while (spawnCounter <= 0 && enemiesToSpawn > 0) {

				i1 = Random.Range (0, newLevel.waves [wave].enemyCounts.Count);
				//Debug.Log (i1 + ", " + spawnedEnemies.Count);
				if (spawnedEnemies [i1] < newLevel.waves [wave].enemyCounts [i1]) {
					enemyCounter++;
					enemiesToSpawn--;
					spawnCounter += spawnDelay;
					spawnedEnemies [i1]++;
					AddEnemy (i1);
				}
				//AddEnemy (0); //Should be based on wave data

				//I'm commenting this out because I don't think it does anything.  If it breaks something, I'll put it back in
				/*if (lastSpawn == enemySpawners.Count - 1) {
					lastSpawn = 0;
				} else {
					lastSpawn++;
				}*/
			}
		}
		 
		//--------------------------------------------------Wave Control
		if (enemyGOs.Count <= 0) {
			if (waveDelay >= 0) {
				waveDelay -= Time.deltaTime;
				if (resource < buildingCost && waveDelay > 5) {
					waveDelay = 5;
				}
			} else {
				waveDelay = 30;
				wave++;
				if (newLevel.arcade || wave < newLevel.waves.Count) {
					if (wave < newLevel.waves.Count) {
						spawnDelay = newLevel.waves [wave].spawnDelay;
						cameraPosition = newLevel.waves [wave].cameraPosition;
						cameraTimer = 5;
						waveSpawnPoint.x = newLevel.waypoints [newLevel.waves [wave].startingWaypoint].pos.x;
						waveSpawnPoint.y = 0;
						waveSpawnPoint.z = newLevel.waypoints [newLevel.waves [wave].startingWaypoint].pos.y;
						waveEnemies = 0;
						spawnedEnemies = new List <int> ();
						for (i1 = 0; i1 < newLevel.waves [wave].enemyTypes.Count; i1++) {
							waveEnemies += newLevel.waves [wave].enemyCounts [i1];
							spawnedEnemies.Add (0);
						}
					} else {
						spawnDelay = newLevel.waves [newLevel.waves.Count - 1].spawnDelay;
						if (lastSpawn == 0) {
							lastSpawn = newLevel.waves [newLevel.waves.Count - 1].enemyCounts [0];
						}
						waveEnemies = Mathf.RoundToInt ((float)lastSpawn * 1.15f); //Sets enemies for next wave
						lastSpawn = waveEnemies;
					}
					enemiesToSpawn += waveEnemies; //Starts the next wave
					waveEnemies = Mathf.RoundToInt ((float)waveEnemies * 1.15f); //Sets enemies for next wave
					waveDisplay.text = "Wave " + wave.ToString ();
				} else {
					gameEnded = true;
				}
			}
		}

		//--------------------------------------------------Controls Enemies
		for (i1 = 0; i1 <  enemyGOs.Count; i1++) {
			//Enemy Movement

			v2a = enemyPos [i1];
			v2b.x = enemyGOs [i1].transform.position.x;
			v2b.y = enemyGOs [i1].transform.position.z;
			if (newLevel.waves [wave].enemyTypes [enemyIDs [i1]].flying) {
				tempVector3.y = 1;
			} else {
				CheckEnemyFacingDirection (v2b.x, enemytargets [i1].x, enemyGOs [i1].transform);
				tempVector3.y = 0;
			}
			v2b = Vector2.MoveTowards (v2b, enemytargets [i1], newLevel.waves [wave].enemyTypes [enemyIDs[i1]].speed * Time.deltaTime);
			tempVector3.x = v2b.x;
			tempVector3.z = v2b.y;
			enemyGOs [i1].transform.position = tempVector3;
			enemyPos [i1] = v2b;
			enemyStepDistance [i1] += (v2a - v2b).magnitude;
			if (enemyStepDistance [i1] >= newLevel.waves [wave].enemyTypes[enemyIDs[i1]].stepDistance) {
				enemyStepDistance [i1] -= newLevel.waves [wave].enemyTypes[enemyIDs[i1]].stepDistance;
				enemySteps [i1]++;
				if (enemySteps [i1] >= newLevel.waves [wave].enemyTypes [enemyIDs[i1]].walkSprites.Count) {
					enemySteps [i1] = 1;
				}
				enemyRenderers [i1].material = newLevel.waves [wave].enemyTypes [enemyIDs[i1]].walkSprites [enemySteps [i1]];
			}
			if ((enemyPos [i1] - enemytargets [i1]).magnitude < .2) {
				if ((enemyPos [i1] - towerPosition).magnitude < 2) {
					DamageTower (enemyHPs [i1]);
					DestroyEnemy (i1); //Removes enemy without death animation
					i1--;
					continue;
				} else if (enemyWaypoints [i1] > 0) {
					enemyWaypoints [i1] = newLevel.waypoints [enemyWaypoints [i1]].root;
					enemytargets [i1] = newLevel.waypoints [enemyWaypoints [i1]].pos + OffsetV2 (newLevel.waypoints [enemyWaypoints [i1]].width);
				} else {
					enemyWaypoints [i1]++;
					enemytargets [i1] = towerPosition;
				}
			} else {
				//Should be for loop between characters, but for now we only have one
				if (newLevel.waves [wave].enemyTypes [enemyIDs [i1]].flying) {
					continue; //If they are flying, the only building they can hit is the main tower, which is checked earylier.  If we make it so players can hit them, then we'll have to add something here.
				}
				i2 = 0;
				for (i2 = 0; i2 < players.Count; i2++) {
					if (players [i2].hp > 0 && (enemyPos [i1] - players [i2].v2).magnitude < .5) {
						players [i2].hp -= enemyHPs [i1];
						if (players [i2].hp <= 0) {
							if (lives > 0) { //If there is an extra life, then they can respawn.
								lives--;
								players [i2].hp = 100;
								livesDisplay.text = "Lives: " + lives;
								players [i2].v2 = newLevel.playerStartingPoints [i1];
								tempVector3.x = players [i2].v2.x;
								tempVector3.y = 0;
								tempVector3.z = players [i2].v2.y;
								players [i2].transform.position = tempVector3;
							} else { //Else, game over, man
								//We'll let the wave continue, in case their defenses are enough to win
								//Should remove them from playable characters
								players [i2].walkSprites [players [i2].step].SetActive (false);
							}
						}
						DestroyEnemy (i1); //Removes enemy without death animation
						i1--;
						i2 = players.Count;
					}
				}
				

				for (i2 = 0; i2 < buildings.Count; i2++) {
					if (enemyPos.Count <= i1) {
						Debug.Log (enemyPos.Count + ", " + i1);
					}

					if (buildings.Count <= i2) {
						Debug.Log (buildings.Count + ", " + i2);
					}
					if ((enemyPos [i1] - buildings [i2].v2).magnitude < .5f) {
						buildings [i2].hp -= enemyHPs [i1];
						if (buildings [i2].hp <= 0) {
							Destroy (buildings [i2].gameObject);
							buildings.RemoveAt (i2);
							i2--;
						}
						DestroyEnemy (i1); //Removes enemy without death animation
						i1--;
					}
				}
	
			}
		}

		//--------------------------------------------------Controls Buildings
		for (i1 = 0; i1 < buildings.Count; i1++) {
			if (buildings [i1].timeout > 0) {
				buildings [i1].timeout -= Time.deltaTime;
				continue;
			}
			if (buildings [i1].id == 1) { //Regular ground building
				if (Ground360Attack (buildings [i1].v2, groundBuildingDamages [buildings [i1].level], groundBuildingRanges [buildings [i1].level])) {
					buildings [i1].timeout = groundBuildingAttackDelays [buildings [i1].level];
				} else {
					buildings [i1].timeout = 1;
				}
			} else if (buildings [i1].id == 2) { //Heal tower
				if (Ground360Heal (buildings [i1].v2, groundBuildingDamages [buildings [i1].level], groundBuildingRanges [buildings [i1].level])) {
					buildings [i1].timeout = groundBuildingAttackDelays [buildings [i1].level];
				} else {
					buildings [i1].timeout = 1;
				}
			}
		}

		//--------------------------------------------------Controls Arrows
		for (i1 = 0; i1 < arrows.Count; i1++) {
			if (!arrows [i1].gameObject.activeSelf) {
				continue;
			}

			if (arrows [i1].airTime > arrows [i1].totalAirTime) {
				arrows [i1].gameObject.SetActive (false);
				arrows [i1].airTime = 0;
			} else {
				arrows [i1].airTime += Time.deltaTime;
				arrows [i1].transform.position = Vector3.Lerp (arrows [i1].startPosition, arrows [i1].target.position, arrows [i1].airTime / arrows [i1].totalAirTime);
			}
		}

		//--------------------------------------------------Controls Dead Enemies
		for (i1 = 0; i1 < deadEnemyGOs.Count; i1++) {
			if (i1 >= enemyDeathTimer.Count) {
				Debug.Log ("i1 is " + i1 + ", but enemyDeathTimer.Count is " + enemyDeathTimer.Count);
			} else if (enemyDeathTimer [i1] < .2f) {
				enemyDeathTimer [i1] += Time.deltaTime;
				tempVector3.x = deadEnemytargets [i1].x;
				tempVector3.y = deadEnemytargets [i1].y;
				tempVector3.z = deadEnemyPos [i1].y;
				deadEnemyGOs [i1].transform.position = Vector3.Lerp (new Vector3 (deadEnemyPos [i1].x, 0, deadEnemyPos [i1].y), tempVector3, enemyDeathTimer [i1] * 5);
			} else {
				RemoveEnemy (i1);
				i1--;
			}
		}
	}

	public void AddCharacter () {
		tempObject = (GameObject)Instantiate (mainCharacter.gameObject, Vector3.zero, Quaternion.identity);
		j1 = players.Count;
		players.Add (tempObject.GetComponent <Character> ()); //Should be based on character select
		if (newLevel.playerStartingPoints.Count < j1) {
			newLevel.playerStartingPoints.Add (newLevel.playerStartingPoints [0]);
			Debug.Log ("Didn't have enough player starting points");
		}
		players [j1].v2 = newLevel.playerStartingPoints [j1];
		tempVector3.x = players [j1].v2.x;
		tempVector3.y = 0;
		tempVector3.z = players [j1].v2.y;
		players [j1].transform.position = tempVector3;
		selectedBuildings.Add (0);
		tempBuildings.Add (null);
	}

	void AddEnemy (int id) {
		tempMesh = NewMesh ();
		tempMesh.transform.localScale = new Vector3 (newLevel.waves [wave].enemyTypes [id].size, newLevel.waves [wave].enemyTypes [id].size, newLevel.waves [wave].enemyTypes [id].size);
		tempMesh.transform.position = waveSpawnPoint + Offset (newLevel.waypoints [newLevel.waves [wave].startingWaypoint].width);
		tempMesh.material = newLevel.waves [wave].enemyTypes [id].walkSprites [0];
		tempMesh.gameObject.SetActive (true);
		enemyGOs.Add (tempMesh.gameObject);
		enemyRenderers.Add (tempMesh);
		enemyWaypoints.Add (newLevel.waves [wave].startingWaypoint);
		if (newLevel.waves [wave].enemyTypes [id].flying) { //If they fly, they'll just move straight to thier objective
			//Should check thier objective, but for now we'll assume it's the tower.
			enemytargets.Add (towerPosition);
			CheckEnemyFacingDirection (tempMesh.transform.position.x, towerPosition.x, tempMesh.transform);
		} else {
			enemytargets.Add (newLevel.waypoints [newLevel.waypoints [newLevel.waves [wave].startingWaypoint].root].pos + OffsetV2 (newLevel.waypoints [enemyWaypoints [i1]].width));
		}
		enemyStepDistance.Add (0);
		enemySteps.Add (0);
		enemyHPs.Add (newLevel.waves [wave].enemyTypes [id].hp);
		enemyPos.Add (new Vector2 (0,0));
		enemyIDs.Add (id);
	}

	void KillEnemy (int enemy) { //Makes the enemy dead
		deadEnemyGOs.Add (enemyGOs [enemy]);
		deadEnemyPos.Add (enemyPos [enemy]);
		deadEnemytargets.Add (enemytargets [enemy]);
		enemyDeathTimer.Add (.001f);
		enemyGOs.RemoveAt (enemy);
		enemyRenderers.RemoveAt (enemy);
		enemyStepDistance.RemoveAt (enemy);
		enemyWaypoints.RemoveAt (enemy);
		enemySteps.RemoveAt (enemy);
		enemyHPs.RemoveAt (enemy);
		enemyPos.RemoveAt (enemy);
		enemytargets.RemoveAt (enemy);
		enemyIDs.RemoveAt (enemy);
	}

	void RemoveEnemy (int enemy) { //Removes an already dead enemy
		//Destroy (deadEnemyGOs [enemy]);
		RemoveMesh (deadEnemyGOs [enemy].GetComponent <MeshRenderer> ());
		deadEnemyGOs.RemoveAt (enemy);
		enemyDeathTimer.RemoveAt (enemy);
		deadEnemyPos.RemoveAt (enemy);
		deadEnemytargets.RemoveAt (enemy);
	}

	void DestroyEnemy (int enemy) { //Removes an alive enemy
		//Destroy (enemyGOs [enemy]);
		RemoveMesh (enemyRenderers [enemy]);
		enemyGOs.RemoveAt (enemy);
		enemyRenderers.RemoveAt (enemy);
		enemyWaypoints.RemoveAt (enemy);
		enemySteps.RemoveAt (enemy);
		enemyHPs.RemoveAt (enemy);
		enemyPos.RemoveAt (enemy);
		enemytargets.RemoveAt (enemy);
		enemyIDs.RemoveAt (enemy);
	}

	MeshRenderer NewMesh () {
		if (meshes.Count > 0) {
			tempMesh = meshes [0];
			meshes.RemoveAt (0);
			return tempMesh;

		} else {
			tempObject = (GameObject)Instantiate (model, Vector3.zero, Quaternion.identity);
			tempObject.transform.rotation = Quaternion.Euler (new Vector3 (30, 0, 0));
			//tempObject.transform.parent = transform;
			return tempObject.GetComponent <MeshRenderer> ();
		}
	}

	void RemoveMesh (MeshRenderer mesh) {
		mesh.gameObject.SetActive (false);
		meshes.Add (mesh);
	}

	public Vector2 RandomTarget () {
		tempVector2.x = Random.Range (-xLimit, xLimit);
		tempVector2.y = Random.Range (-yLimit, yLimit);
		return tempVector2;
	}

	public Vector3 Offset (float offset) {
		tempVector3.x = Random.Range (-offset, offset);
		tempVector3.y = 0;
		tempVector3.z = Random.Range (-offset, offset);
		return tempVector3;
	}

	public Vector2 OffsetV2 (float offset) {
		tempVector2.x = Random.Range (-offset, offset);
		tempVector2.y = Random.Range (-offset, offset);
		return tempVector2;
	}

	void CheckEnemyFacingDirection (float ourX, float theirX, Transform ourTransform) {
		if (ourX > theirX) { //They are to our left, so we should also be facing left
			if (ourTransform.localScale.x > 0) { //Checks if we are facing right
				ourTransform.localScale = new Vector3 (ourTransform.localScale.x * -1f, ourTransform.localScale.y, ourTransform.localScale.z);
			}
		} else if (ourTransform.localScale.x < 0) {
			ourTransform.localScale = new Vector3 (ourTransform.localScale.x * -1f, ourTransform.localScale.y, ourTransform.localScale.z);

		}
	}

	void DamagePlayer (int player, int dmg) {

	}

	public void MakeArrow (Vector3 startingPoint, Transform target) {
		tempBool = false;
		for (j1 = 0; j1 < arrows.Count; j1++) {
			if (arrows [j1].gameObject.activeSelf) {
				tempBool = true;
				arrows [j1].startPosition = startingPoint;
				arrows [j1].target = target;
				arrows [j1].transform.position = startingPoint;
				if (startingPoint.x < target.position.x) { //It should be facing right
					if (arrows [j1].transform.localScale.x < 0) { //Checks if it is currently facing left
						arrows [j1].transform.localScale = new Vector3 (arrows [j1].transform.localScale.x * -1, 1, 1);
					}
				} else if (startingPoint.x > target.position.x) { //It should be facing left
					if (arrows [j1].transform.localScale.x > 0) { //Checks if it is currently facing right
						arrows [j1].transform.localScale = new Vector3 (arrows [j1].transform.localScale.x * -1, 1, 1);
					}
				}
				arrows [j1].gameObject.SetActive (true);
			}
		}
	}

	bool Ground360Heal (Vector2 v2, int heal, float range) {
		bool tempBool2 = false; //saves weather or not they healed a player
		for (j1 = 0; j1 < players.Count; j1++) {
			if ((players [j1].v2 - v2).magnitude <= range) {
				players [j1].hp += heal;
				if (players [j1].hp > 100) {
					players [j1].hp = 100;
				}
				tempBool2 = true;
			}
		}
		return tempBool2;

	}

	bool Ground360Attack (Vector2 v2, int attack, float range) { //Should be renamed to turret attack
		bool tempBool2 = false; //Stores if we found an enemy
		for (j1 = 0; j1 < enemyGOs.Count; j1++) {
			if ((enemyPos [j1] - v2).magnitude <= range) {
				if (enemyHPs [j1] <= attack) {
					AddResource (newLevel.waves [wave].enemyTypes [enemyIDs[j1]].worth);
					if (v2.x < enemyPos [j1].x) {
						v2a.x = enemyPos [j1].x + .25f;
					} else {
						v2a.x = enemyPos [j1].x - .25f;
					}
					v2a.y = .5f;
					enemytargets [j1] = v2a;
					KillEnemy (j1);
				} else {
					enemyHPs [j1] -= attack;
				}
				return true;
			}
		}
		return tempBool2;
	}

	public void NormalAttack (int player) {
		int attackLeft = 10;
		float minX, maxX, minY, maxY;
		float range = 1.5f;
		bool attackRight;
		if (players [player].transform.localScale.x < 0) { //They are looking to the left
			attackRight = false;
			maxX = players [player].v2.x;
			minX = players [player].v2.x - range;
		} else { //they are looking to the right
			attackRight = true;;
			minX = players [player].v2.x;
			maxX = players [player].v2.x + range;
		}
		minY = players [player].v2.y - range;
		maxY = players [player].v2.y + range;
		for (j1 = 0; j1 < enemyGOs.Count; j1++) {
			if (newLevel.waves [wave].enemyTypes [enemyIDs [j1]].flying) { //If they are in the air, we cant hit them
				continue; //Moves on to the next enemy
			}
			if (enemyPos [j1].x < minX) {
				continue;
			} else if (enemyPos [j1].x > maxX) {
				continue;
			} else if (enemyPos [j1].y < minY) {
				continue;
			} else if (enemyPos [j1].y > maxY) {
				continue;
			}
			//Now we know they are inside a box of 5
			//Now we know that this enemy is on the ground and in front of us.  Time to do a distance check

			if ((enemyPos [j1] - players [player].v2).magnitude <= 3) { //If they are within 3 meters of us, we hit them
				if (enemyHPs [j1] < attackLeft) {
					attackLeft -= enemyHPs [j1];
					AddResource (newLevel.waves [wave].enemyTypes[0].worth);
					if (attackRight) {
						v2a.x = enemyPos [j1].x + .25f;
					} else {
						v2a.x = enemyPos [j1].x - .25f;
					}
					v2a.y = .5f;
					enemytargets [j1] = v2a;
					KillEnemy (j1);
					j1--;
				} else {
					enemyHPs [j1] -= attackLeft;
					return; //Ends attack
				}
			}
		}
	}

	void AddResource (int addedResource) {
		resource += addedResource;
		resourceDisplay.text = "Resource: " + resource.ToString ();
	}

	void RemoveResource (int removedResource) {
		resource -= removedResource;
		resourceDisplay.text = "Resource: " + resource.ToString ();
	}

	void DamageTower (int dmg) {
		towerHealth -= dmg;
		if (towerHealth <= 0) {
			Destroy (towerGO);
			towerDisplay.text = "Tower: Broken";
		} else {
			towerDisplay.text = "Tower: " + towerHealth + "hp";
		}
	}

	float Elevation (Vector2 v2) {
		float d = 0;
		for (j1 = 0; j1 < elevators.Count; j1++) {
			d = (v2 - elevators [j1].v2).magnitude;
			if (elevators [j1].round) {
				if (d <= elevators [j1].v3.x) {
					if (elevators [j1].id == 1) { //platform
						return elevators [j1].v3.y;
					} else if (elevators [j1].id == 2) { //Mound
						return Mathf.Sqrt ((elevators [j1].v3.x * elevators [j1].v3.x) - (d * d));
					} else if (elevators [j1].id == 3) { //Concave
						return -Mathf.Sqrt ((elevators [j1].v3.x * elevators [j1].v3.x) - (d * d));
					} else if (elevators [j1].id == 4) { //Cone
						return elevators [j1].v3.x - d; //The closer they are, the higher they will be
					} else if (elevators [j1].id == 5) { //Funnel
						return d - elevators [j1].v3.x; //The closer they are, the lower they will be
					}
				}
			} else { //We now have to do a rectangular distance check 
				if (v2.x >= elevators [j1].v3.x + elevators [j1].v2.x && v2.x <= elevators [j1].v3e.x + elevators [j1].v2.x && v2.y >= elevators [j1].v3.y + elevators [j1].v2.y && v2.y <= elevators [j1].v3e.y + elevators [j1].v2.y) {
					if (elevators [j1].id == 1) { //Floor platform.  We don’t care about depth
						return elevators [j1].v3e.z;
					} else if (elevators [j1].id == 5) { //Northern Ramp. 
						//return 
					}
				}
			}
		}
		return 0; //Should only return 0 if no other element returns a value
	}

	void RestartGame () {
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name, LoadSceneMode.Single);
	}
}