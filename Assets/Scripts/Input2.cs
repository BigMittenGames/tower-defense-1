﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class Input2 : MonoBehaviour {

	public static Input2 _instance = null;
	public static Input2 Instance {
		get {
			return _instance;
		}
	}
	public Input2() : base() {
		_instance = this;
	}

	int i1, j1;
	Joystick tempJoystick;
	int keyboard = -1;

	public float x, y;
	public bool attack, jump; //Jump should cancel
	public bool buildUp, buildDown, buildLeft, buildRight;
	Rewired.Player player;


	public bool PressingAnyKey () {
		IList<Joystick> joysticks = ReInput.controllers.Joysticks;
		for(i1 = 0; i1 < joysticks.Count; i1++) {
			if(joysticks[i1].GetAnyButtonDown()) {
				return true; //If any joystick is pressing any button, then it returns true and ends the frame.
			}
		}
		return false;
	}

	public int NewPlayerPress () {
		IList<Joystick> joysticks = ReInput.controllers.Joysticks;
		if (keyboard == -1) { //No one has claimed the keyboard
			if (ReInput.controllers.Keyboard.GetAnyButton ()) {

			}
		}
		for(i1 = 0; i1 < joysticks.Count; i1++) {

			tempJoystick = joysticks[i1];
			if(ReInput.controllers.IsControllerAssigned(tempJoystick.type, tempJoystick.id)) continue; // joystick is already assigned to a Player

			// Chec if a button was pressed on the joystick
			if(tempJoystick.GetAnyButtonDown()) {

				// Find the next Player without a Joystick
				Player player = FindPlayerWithoutJoystick();
				if (player != null) { // no free joysticks

					// Assign the joystick to this Player
					player.controllers.AddController (tempJoystick, false);
					return i1; //i1 is the character number that was just added
				}
			}
		}
		return -1; //This will only return if no player is found.
	}

	public Player FindPlayerWithoutJoystick() {
		IList<Player> players = ReInput.players.Players;
		for(j1 = 0; j1 < players.Count; j1++) {
			if (j1 == keyboard) continue;
			if(players[j1].controllers.joystickCount > 0) continue;
			return players[j1];
		}
		return null;
	}

	public void UpdatePlayerInputs (int p) {
		player = ReInput.players.GetPlayer (p);
		x = player.GetAxis("Move Horizontal");
		y = player.GetAxis("Move Vertical");
		attack = player.GetButtonDown("Attack");
		buildUp = player.GetButtonDown("BuildUp");
		buildDown = player.GetButtonDown("BuildDown");
		buildLeft = player.GetButtonDown("BuildLeft");
		buildRight = player.GetButtonDown("BuildRight");
	}
}
