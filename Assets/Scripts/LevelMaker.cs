﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelMaker : MonoBehaviour {

	public Level1 level;
	public Transform waypointParent;

	void Start () {
		level.waypoints = new List <Level1.Waypoint> ();
		for (int i = 0; i < waypointParent.childCount; i++) {
			level.waypoints.Add (new Level1.Waypoint ());
			if (i >= 2) {
				level.waypoints [i].root = i - 1;
			}
			level.waypoints [i].width = waypointParent.GetChild (i).localScale.x * .4f;
			level.waypoints [i].pos = new Vector2 (waypointParent.GetChild (i).position.x, waypointParent.GetChild (i).position.z);
		}
	}
}
