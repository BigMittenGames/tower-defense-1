﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Enemy", menuName = "Enemy", order = 500)]
public class Enemy : ScriptableObject {

	public int id = 1;
	public float stepDistance, speed = 1, size = 1;
	public int hp, worth;
	public GameObject prefab;
	public Material hitGraphic;
	public List <Material> walkSprites;
	public bool flying, armored;
}
