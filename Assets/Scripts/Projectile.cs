﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	public Transform target;
	public Vector3 startPosition;
	public float airTime;
	public float totalAirTime;
}