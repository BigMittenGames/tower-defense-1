﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level", menuName = "Level", order = 501)]
public class Level1 : ScriptableObject {

	/*[System.Serializable]
	public class Cluster {
		[SerializeField] public List <int> startingWaypoints;
		[SerializeField] public float spawnDelay = .1f;
		[SerializeField] public List <Enemy> enemyTypes;
		[SerializeField] public List <int> enemyCounts;
	}*/

	[System.Serializable]
	public class Wave {
		[SerializeField] public int startingWaypoint = 1;
		[SerializeField] public float spawnDelay = .1f;
		[SerializeField] public List <Enemy> enemyTypes;
		[SerializeField] public List <int> enemyCounts;
		[SerializeField] public Vector4 playerArea = new Vector4 (-1600, 1600, -900, 900);
		[SerializeField] public Vector3 cameraPosition = new Vector3 (0, 10, -10);
	}

	[System.Serializable]
	public class Waypoint {
		[SerializeField] public int root;
		[SerializeField] public float width = .5f;
		[SerializeField] public Vector2 pos;
	}

	public bool arcade;
	public Sprite groundSprite;
	public Vector2 groundSize;
	public Vector2 towerPosition;
	public int music;
	public List <Wave> waves;
	public List <Waypoint> waypoints;
	public List <Vector2> decorationLocations;
	public List <Vector3> playerStartingPoints;
	//Add Evelvators, somehow.
}
