﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class LevelSelect : MonoBehaviour {

	Main main;

	bool levelSelected, activePlayer;
	List <Level1> levels = new List <Level1> {};
	int i1, selectedLevel = 0;

	void Start () {
		main = gameObject.GetComponent <Main> ();
		i1 = 0;
		foreach (Level1 l in Resources.FindObjectsOfTypeAll(typeof(Level1)) as Level1[]) {
			levels.Add (l);
			i1++;
		}
		main.bigText.text = "Press 'A' or 'X' to join";
	}

	void Update () {
		if (levelSelected) {
			gameObject.GetComponent <LevelSelect> ().enabled = false;
			return;
		}
		if (!activePlayer) {
			i1 = Input2.Instance.NewPlayerPress ();
			if (i1 != -1) {
				activePlayer = true;
				//main.bigText.text = "<- " + levels [selectedLevel].name + " ->";
			}
			return;
		}
		Input2.Instance.UpdatePlayerInputs (0);
		if (Input2.Instance.buildLeft) {
			Debug.Log ("Move Left");
			if (selectedLevel == 0) {
				selectedLevel = levels.Count;
			}
			selectedLevel--;
			//main.bigText.text = "<- " + levels [selectedLevel].name + " ->";
		} else if (Input2.Instance.buildRight) {
			Debug.Log ("Move Right");
			selectedLevel++;
			if (selectedLevel >= levels.Count) {
				selectedLevel = 0;
			}
			//main.bigText.text = "<- " + levels [selectedLevel].name + " ->";
		}
		main.bigText.text = "<- " + levels [selectedLevel].name + " ->";


		if (Input2.Instance.attack) {
			main.newLevel = levels [selectedLevel];
			main.bigText.gameObject.SetActive (false);
			main.enabled = true;
			main.AddCharacter ();
			levelSelected = true;
		}
	}
}
