﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour {

	public int id;
	public int level;
	public int hp;
	public Vector2 v2;
	public float timeout = 0;
	public List <GameObject> levels;
}
