﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Elevator", menuName = "Elevator", order = 1)]
public class Elevator : ScriptableObject {

	public bool round = true;
	[Tooltip("1 = Platform")]
	public int id;
	public Vector2 v2;
	public Vector3 v3, v3e;
}
