﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable] public class SaveState : ScriptableObject {

	public int highScore;
}
