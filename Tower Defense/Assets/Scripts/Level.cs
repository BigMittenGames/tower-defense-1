﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour {

	public List <float> enemySpawnDelays;
	public List <float> waypointWidths;
	public int music;
	public int wavesToWin = 0; //If set to zero, the waves will never stop
	public List <int> waveStartingPoints;
	public List <int> decorations;
	public List <int> waypointRoots;
	public List <string> waveContents;
	public List <Vector2> decorationLocations;
	public List <Vector2> waypointLocations;
	public List <Vector2> playerBoundries;
	public List <Vector3> cameraPositions;
	public Vector2 startingPoint;
	public List <Elevator> elevators;
}
